import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Login from '../pages/Login'
import About from '../pages/About'
import Skills from '../pages/Skills'
import { BottomNavigator } from '../component'
import Register from '../pages/Register'
import Splash from '../pages/Splash'
import Detail from '../pages/Detail'

const Stack = new createStackNavigator()
const Tab = new createBottomTabNavigator()

const MainApp = () => {
    return(
        <Tab.Navigator initialRouteName="About" tabBar={props => <BottomNavigator {...props} />}>
            <Tab.Screen name="About" component={About} options={{headerShown: false}} />
            <Tab.Screen name="Skills" component={Skills} options={{headerShown: false}} />
        </Tab.Navigator>
    )
}

const Router = () => {
    return (
        <Stack.Navigator initialRouteName="Splash">
            <Stack.Screen name="Splash" component={Splash} options={{headerShown: false}}></Stack.Screen>
            <Stack.Screen name="Login" component={Login} options={{headerShown: false}}></Stack.Screen>
            <Stack.Screen name="Register" component={Register} options={{headerShown: false}}></Stack.Screen>
            <Stack.Screen name="Detail" component={Detail} options={{headerShown: false}}></Stack.Screen>
            <Stack.Screen name="MainApp" component={MainApp} options={{headerShown: false}}></Stack.Screen>
        </Stack.Navigator>
    )
}

export default Router

const styles = StyleSheet.create({})
