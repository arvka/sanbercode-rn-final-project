import { createStore } from "redux"

const initialState = {
    loading: false,
    userdata: {}
}

const reducer = (state = initialState, action) => {
    if(action.type === 'SET_LOADING'){
        return {...state, loading: action.value}
    }
    if(action.type === 'SET_USERDATA'){
        return {...state, userdata: action.value}
    }
    if(action.type === 'DELETE_USERDATA'){
        return {...state, userdata: {}}
    }
    return state
}

const store = createStore(reducer)

export default store