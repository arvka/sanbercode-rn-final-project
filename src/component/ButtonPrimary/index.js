import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import colors from '../../utils/colors'

const ButtonPrimary = ({onPress, btnText, small}) => {
    return (
        <View style={{flex: 1}}>
            <TouchableOpacity  style={styles.container} onPress={onPress} >
                <Text style={styles.btnText(small)}>{btnText}</Text>
            </TouchableOpacity>
        </View>
    )
}

export default ButtonPrimary

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.primary,
        padding: 10,
        borderRadius: 10
    },
    btnText: (small) => ({
        textAlign: 'center',
        fontSize: small ? 12 : 18,
        fontWeight: '600',
        color: colors.bgWhite
    })
})
