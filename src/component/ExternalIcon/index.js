import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

const ExternalIcon = ({name, size, color}) => {
    switch (name) {
        case 'php':
            return (<MaterialCommunityIcons name="language-php" size={size} color={color} />)
        case 'javascript':
            return (<MaterialCommunityIcons name="language-javascript" size={size} color={color} />)
        case 'fire-ci':
            return (<MaterialCommunityIcons name="fire" size={size} color={color} />)
        case 'nodejs' :
            return (<MaterialCommunityIcons name="nodejs" size={size} color={color} />)
        case 'git' :
            return (<MaterialCommunityIcons name="git" size={size} color={color} />)
        case 'api' :
            return (<MaterialCommunityIcons name="api" size={size} color={color} />)
        case 'gitlab' :
            return (<MaterialCommunityIcons name="gitlab" size={size} color={color} />)
        case 'github' :
            return (<AntDesign name="github" size={size} color={color} />)
        case 'linkedin' :
            return (<AntDesign name="linkedin-square" size={size} color={color} />)
        case 'gmail' :
            return (<MaterialCommunityIcons name="gmail" size={size} color={color} />)
        case 'user' :
            return (<FontAwesome name="user-circle" size={size} color={color} />)
        case 'star-primo' :
            return (<MaterialCommunityIcons name="star-four-points" size={size} color={color} />)
        default:
            return false
    }
}

export default ExternalIcon

const styles = StyleSheet.create({})
