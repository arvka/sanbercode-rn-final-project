import React from 'react'
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native'
import colors from '../../utils/colors'

const Loading = () => {
    return (
        <View style={styles.wrapper}>
            <ActivityIndicator size="large" color={colors.primary} />
            <Text style={styles.loadingText}>Loading</Text>
        </View>
    )
}

export default Loading

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        backgroundColor: colors.loadingBackground
    },
    loadingText:{
        marginTop: 10,
        fontSize: 18,
        fontWeight: '600',
        color: colors.secondary
    }
})
