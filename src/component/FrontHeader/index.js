import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import colors from '../../utils/colors'

const FrontHeader = () => {
    return (
        <View style={{display: 'flex', flexDirection: 'column', alignItems: 'flex-end', width: 'auto'}}>
            <Text style={styles.titleText}>SanberApp</Text>
            <Text style={styles.subtitleText}>Portofolio</Text>
        </View>
    )
}

export default FrontHeader

const styles = StyleSheet.create({
    titleText:{
        fontSize: 36,
        fontWeight: '600',
        color: colors.primary
    },
    subtitleText:{
        fontSize: 24,
        fontWeight: '600',
        color: colors.secondary
    }
})