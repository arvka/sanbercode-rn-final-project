import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { Gap } from '..'
import colors from '../../utils/colors'

const ContentHeader = () => {
    return (
        <View style={styles.boxHeader}>
            <Gap height={26} />
            <Text style={styles.headerText}>SanberApp Portofolio</Text>
        </View>
    )
}

export default ContentHeader

const styles = StyleSheet.create({
    boxHeader: {
        height: 150,
        width: '100%',
        borderBottomStartRadius: 20,
        borderBottomEndRadius: 20,
        backgroundColor: colors.primary,
        display: 'flex',
        alignItems: 'center'
    },
    headerText: {
        color: colors.bgWhite,
        fontWeight: '600',
        fontSize: 18
    },
})
