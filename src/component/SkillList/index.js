import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import colors from '../../utils/colors'
import ExternalIcon from '../ExternalIcon'

const SkillList = ({skillName, iconName, skillLevel, percent, onPress}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
                <View style={styles.percentageBar(percent)}>
                </View>
                <View style={styles.skillWrapper}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <View style={{marginHorizontal: 14}}>
                            <ExternalIcon name={iconName} size={40} color="black" />
                        </View>
                        <View style={{flexDirection: 'column'}}>
                            <Text style={styles.textSkill}>{skillName}</Text>
                            <Text style={styles.textLevel}>{skillLevel}</Text>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={styles.textPercent}>{percent}</Text>
                    </View>
                </View>
        </TouchableOpacity>
    )
}

export default SkillList

const styles = StyleSheet.create({
    container:{
        width: '100%',
        flexDirection: 'row'
    },
    percentageBar : (percent) => (
        {
            position: 'absolute',
            backgroundColor: colors.secondary,
            opacity: 0.3,
            height: '100%',
            width: percent
        }
    ),
    skillWrapper:{
        flex: 1,
        paddingVertical: 8,
        borderBottomColor: colors.borderGrey,
        borderBottomWidth: 2,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    textSkill: {
        color: colors.primary,
        fontSize: 18,
        fontWeight: '600'
    },
    textLevel:{
        color: colors.primary,
        fontSize: 14,
        fontWeight: '400'
    },
    textPercent:{
        color: colors.primary,
        fontSize: 20,
        fontWeight: '600',
        marginRight: 17
    }
})
