import React, { useState } from 'react'
import { StyleSheet, Text, View, TextInput } from 'react-native'
import colors from '../../utils/colors'

const InputText = ({value, onChangeText, masked, placeholder}) => {
    const [border, setBorder] = useState(colors.borderGrey)
    const onFocusInput = () => {
        setBorder(colors.secondary)
    }
    const onBlurInput = () => {
        setBorder(colors.borderGrey)
    }
    return (
        <View style={styles.container}>
            <TextInput onFocus={onFocusInput} onBlur={onBlurInput} placeholderTextColor={colors.borderGrey} placeholder={placeholder} style={styles.textInput(border)} value={value} onChangeText={onChangeText} secureTextEntry={masked}/>
        </View>
    )
}

export default InputText

const styles = StyleSheet.create({
    textInput: (border) => ({
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: border,
        borderRadius: 10,
        paddingTop: 11,
        paddingBottom: 12,
        paddingHorizontal: 12,
        fontWeight: '400',
        color: colors.black
    }),
    container:{
        width: '100%'
    }
})
