import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import colors from '../../utils/colors'
import ExternalIcon from '../ExternalIcon'

const TabItem = ({title, active, onPress, onLongPress}) => {
    const TabIcon = () =>{
        if(title === "About" && !active){
            return (<ExternalIcon name="user" size={25} color={colors.bgWhite} />)
        }
        if(title === "About" && active){
            return (<ExternalIcon name="user" size={25} color={colors.secondary} />)
        }
        if(title === "Skills" && !active){
            return (<ExternalIcon name="star-primo" size={25} color={colors.bgWhite} />)
        }
        if(title === "Skills" && active){
            return (<ExternalIcon name="star-primo" size={25} color={colors.secondary} />)
        }
    }
    return (
        <View style={{flex: 1}}>
            <TouchableOpacity style={styles.container} onPress={onPress} onLongPress={onLongPress}>
                <TabIcon />
                <Text style={styles.text(active)}>{title}</Text>
            </TouchableOpacity>
        </View>
    )
}

export default TabItem

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
    },
    text: (active) => ({
        color: active ? colors.secondary : colors.bgWhite,
        fontSize: 16,
        textAlign: 'center',
        fontWeight: '600'
    })
})
