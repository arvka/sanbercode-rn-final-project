import React, { useEffect } from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { ButtonPrimary, Gap } from '../component'
import AntDesign from 'react-native-vector-icons/AntDesign'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { ProfileArka } from '../assets'
import colors from '../utils/colors'
import auth from '@react-native-firebase/auth'
import { showError } from '../utils/showMessage'
import { useSelector, useDispatch } from 'react-redux'

const About = ({navigation}) => {
    const logout = () => {
        dispatch({type: 'SET_LOADING', value: true})
        auth().signOut()
                .then(() => {
                    dispatch({type: 'DELETE_USERDATA'})
                    dispatch({type: 'SET_LOADING', value: false})
                    navigation.replace('Login')
                }).catch(error => {
                    console.log(error)
                    showError(error.message)
                })
    }
    const userdata = useSelector(state => state.userdata)
    const dispatch = useDispatch()
    
    return (
        <View style={{justifyContent: 'space-between', height: '100%'}}>
            <ScrollView showsVerticalScrollIndicator style={styles.container}>
                <View style={styles.boxHeader}>
                    <Gap height={16} />
                    <Text style={styles.headerText}>SanberApp Portofolio</Text>
                </View>
                <View style={styles.namecardWrapper}>
                    <Image source={ProfileArka} style={{width: 80, height: 80, borderRadius: 80/2, marginHorizontal: 15}} />
                    <View style={{paddingVertical: 10, flexWrap: 'wrap', flex: 1}}>
                        <Text style={{fontWeight: '600', fontSize: 25}}>Arka Fadila Yasa</Text>
                        <Text style={{fontWeight: '400', fontSize: 16}}>Fullstack Application Developer</Text>
                    </View>
                </View>
                <View style={styles.hellocardWrapper}>
                    <View style={{flexDirection: 'row'}}>
                        <Text style={styles.subtitleContact}>Hello,&nbsp;</Text>
                        <Text style={styles.subtitleContact}>{userdata.username}</Text>
                    </View>
                    <View>
                        <ButtonPrimary btnText="Logout" onPress={logout} small/>
                    </View>
                </View>
                <Text style={styles.sectionTitle}>Portofolio Link</Text>
                <View style={styles.listWrapper}>
                    <View style={styles.listContact}>
                        <AntDesign name="github" size={35} color="black" style={{marginVertical: 5, marginHorizontal: 20}} />
                        <View style={{paddingVertical: 10}}>
                            <Text style={styles.titleContact}>Github</Text>
                            <Text style={styles.subtitleContact}>@arkafyasa</Text>
                        </View>
                    </View>
                    <View style={styles.listContact}>
                        <MaterialCommunityIcons name="gitlab" size={35} color="black" style={{marginVertical: 5, marginHorizontal: 20}} />
                        <View style={{paddingVertical: 10}}>
                            <Text style={styles.titleContact}>Gitlab</Text>
                            <Text style={styles.subtitleContact}>@arvka</Text>
                        </View>
                    </View>
                </View>
                <Text style={styles.sectionTitle}>Contact Me</Text>
                <View style={styles.listWrapper}>
                    <View style={styles.listContact}>
                        <AntDesign name="linkedin-square" size={35} color="black" style={{marginVertical: 5, marginHorizontal: 20}} />
                        <View style={{paddingVertical: 10}}>
                            <Text style={styles.titleContact}>Linkedin</Text>
                            <Text style={styles.subtitleContact}>Arka Fadila Yasa</Text>
                        </View>
                    </View>
                    <View style={styles.listContact}>
                        <MaterialCommunityIcons name="gmail" size={35} color="black" style={{marginVertical: 5, marginHorizontal: 20}} />
                        <View style={{paddingVertical: 10}}>
                            <Text style={styles.titleContact}>Gmail</Text>
                            <Text style={styles.subtitleContact}>arkayasa.mail@gmail.com</Text>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default About

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.bgGrey,
        flex: 1
    },
    boxHeader: {
        height: 150,
        width: '100%',
        borderBottomStartRadius: 20,
        borderBottomEndRadius: 20,
        backgroundColor: colors.primary,
        display: 'flex',
        alignItems: 'center'
    },
    headerText: {
        color: colors.bgWhite,
        fontWeight: '600',
        fontSize: 18
    },
    namecardWrapper: {
        backgroundColor: colors.bgWhite,
        marginHorizontal: 15,
        width: 'auto',
        minHeight: 120,
        marginTop: -60,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    hellocardWrapper:{
        backgroundColor: colors.bgWhite,
        marginHorizontal: 15,
        marginVertical: 10,
        borderRadius: 10,
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'space-between',
        alignItems: 'center'
    }, 
    sectionTitle: {
        fontSize: 24,
        fontWeight: '400',
        color: '#000000',
        marginTop: 23,
        marginBottom: 11,
        marginHorizontal: 15
    },
    listWrapper:{
        backgroundColor: colors.bgWhite,
        paddingTop: 5,
        paddingBottom: 15,
        width: '100%',
        minHeight: 100
    },
    listContact: {
        borderBottomColor: colors.borderGrey,
        borderBottomWidth: 1,
        minHeight: 40,
        height: 'auto',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    titleContact: {
        fontWeight: '600',
        fontSize: 18
    },
    subtitleContact: {
        fontWeight: '400',
        fontSize: 16
    }
})
