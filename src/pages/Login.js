import React from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useDispatch } from 'react-redux'
import { FrontHeader, Gap, InputText, ButtonPrimary } from '../component'
import colors from '../utils/colors'
import useForm from '../utils/useForm'
import auth from '@react-native-firebase/auth'
import { showError } from '../utils/showMessage'
import database from '@react-native-firebase/database'

const Login = ({navigation}) => {
    const [form, setForm] = useForm({
        email: '',
        password: ''
    })

    const dispatch = useDispatch()

    const submitLogin = () => {
        console.log(form)
        dispatch({type: 'SET_LOADING', value: true})
        auth().signInWithEmailAndPassword(form.email, form.password)
            .then(success => {
                console.log('success login | ', success)
                database()
                    .ref(`users/${success.user.uid}`)
                    .once('value')
                    .then(resDB => {
                        console.log('Data User | ', resDB.val())
                        const userdata = resDB.val()
                        dispatch({type: 'SET_USERDATA', value: userdata})
                        dispatch({type: 'SET_LOADING', value: false})
                        navigation.replace('MainApp')
                    })
            })
            .catch(error => {
                dispatch({type: 'SET_LOADING', value: false})
                console.log('error login | ', error)
                showError(error.message)
            })
    }

    return (
        <View style={styles.container}>
            <View style={styles.cardWrapper}>
                <Gap height={25} />
                <FrontHeader />
                <Text style={{fontSize: 25, fontWeight: '400', marginTop: 13, marginBottom: 32}}>User Login</Text>
                <InputText placeholder="Email" value={form.email} onChangeText={value => setForm("email", value)} />
                <Gap height={20} />
                <InputText placeholder="Password" value={form.password} onChangeText={value => setForm("password", value)} masked/>
                <Gap height={32} />
                <View style={{flexDirection: 'row'}}>
                    <ButtonPrimary btnText="Login" onPress={submitLogin} />
                </View>
                <TouchableOpacity style={{marginTop: 15, borderBottomColor: colors.secondary, borderBottomWidth: 2}} onPress={ () => {navigation.navigate("Register")}}>
                    <Text style={styles.textRegister}>Register New User</Text>
                </TouchableOpacity>
                <Gap height={25} />
            </View>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.primary,
        display: 'flex',
        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
        padding: 40
    },
    cardWrapper: {
        backgroundColor: colors.bgWhite,
        borderRadius: 20,
        paddingHorizontal: 20,
        width: '100%',
        height: 'auto',
        alignItems: 'center'
    },
    titleText:{
        fontSize: 36,
        fontWeight: '600',
        marginTop: 27,
        color: colors.primary
    },
    subtitleText:{
        fontSize: 24,
        fontWeight: '600',
        color: colors.secondary
    },
    inputText: {
        marginBottom: 30
    },
    textRegister:{
        color: colors.secondary,
        fontSize: 16,
        fontWeight: '400'
    }
})
