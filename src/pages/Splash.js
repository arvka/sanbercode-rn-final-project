import React, {useEffect, useState} from 'react'
import { ActivityIndicator, Image, StyleSheet, Text, View } from 'react-native'
import auth from '@react-native-firebase/auth'
import { AppIcon } from '../assets'
import database from '@react-native-firebase/database'
import { useDispatch } from 'react-redux'

const Splash = ({navigation}) => {
    const dispatch = useDispatch()
    const [userLoading, setUserLoading] = useState(false)
    useEffect(() => {
        const checkLogin = auth().onAuthStateChanged(user => {
            setTimeout(() => {
                setUserLoading(true)
                if(user){
                    console.log('User Logindata | ', user)
                    database()
                        .ref(`users/${user.uid}`)
                        .once('value')
                        .then(resDB => {
                            setUserLoading(false)
                            console.log('Data User | ', resDB.val())
                            const userdata = resDB.val()
                            dispatch({type: 'SET_USERDATA', value: userdata})
                            navigation.replace('MainApp')
                        })
                }else{
                    setUserLoading(false)
                    navigation.replace('Login')
                }
            }, 3000)
        })
        return () => checkLogin()
    }, [navigation])

    return (
        <View style={styles.container}>
            <Image source={AppIcon} style={{width: 150, height: 150}} />
            {userLoading && <>
                <ActivityIndicator size="large" />
                <Text>Loading Userdata</Text>
            </>}
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
