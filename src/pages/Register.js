import React, { useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { FrontHeader, Gap, InputText } from '../component'
import ButtonPrimary from '../component/ButtonPrimary'
import colors from '../utils/colors'
import useForm from '../utils/useForm'
import { useDispatch } from 'react-redux'
import { showError, showSucess } from '../utils/showMessage'
import auth from '@react-native-firebase/auth'
import database from '@react-native-firebase/database'

const Register = () => {
    const [form, setForm] = useForm({
        email: '',
        username: '',
        password: '',
        confirmPassword: ''
    })
    const [showHint, setShowHint] = useState(false)
    const dispatch = useDispatch()

    const submitRegister = () => {
        console.log(form)
        dispatch({type: 'SET_LOADING', value: true})
        if(form.confirmPassword === form.password){
            auth().createUserWithEmailAndPassword(form.email, form.password)
                .then(success => {
                  console.log(success)
                  dispatch({type: 'SET_LOADING', value: false}) 
                  showSucess("User registration data has been saved successfully")
                  setForm('reset')
                  const userData = {
                      email: form.email,
                      username: form.username,
                      uid: success.user.uid
                  }
                  database().ref(`users/${success.user.uid}/`).set(userData)
                })
                .catch(error => {
                    dispatch({type: 'SET_LOADING', value: false}) 
                    showSucess(error.message)
                })
        }else{
            showError("Password and Confirm Password value should be same")
            dispatch({type: 'SET_LOADING', value: false})
        }
    }

    const confirmPassword = (value) => {
        setForm('confirmPassword', value)
        if(value !== form.password){
            setShowHint(true)
        }else{
            setShowHint(false)
        }
    }

    return (
        <View style={styles.container}>
            <View style={styles.cardWrapper}>
                <Gap height={25} />
                <FrontHeader />
                <Text style={{fontSize: 25, fontWeight: '400', marginTop: 13, marginBottom: 22}}>Register</Text>
                <InputText placeholder="Email" value={form.email} onChangeText={value => setForm('email', value)} />
                <Gap height={10} />
                <InputText placeholder="Username" value={form.username} onChangeText={value => setForm('username', value)} />
                <Gap height={10} />
                <InputText placeholder="Password" value={form.password} onChangeText={value => setForm('password', value)} masked />
                <Gap height={10} />
                <InputText placeholder="Confirm Password" value={form.confirmPassword} onChangeText={confirmPassword} masked />
                {showHint && <Text style={{fontSize: 12, fontWeight:'400', color: colors.error, width: '100%'}}>Input doesn't match with password</Text>}
                <Gap height={30} />
                <View style={{flexDirection: 'row'}}>
                    <ButtonPrimary btnText="Register" onPress={submitRegister} />
                </View>
                <Gap height={25} />
            </View>
        </View>
    )
}

export default Register

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.primary,
        display: 'flex',
        justifyContent: 'center',
        flex: 1,
        alignItems: 'center',
        padding: 40
    },
    cardWrapper: {
        backgroundColor: colors.bgWhite,
        borderRadius: 20,
        paddingHorizontal: 20,
        width: '100%',
        height: 'auto',
        alignItems: 'center'
    },
    titleText:{
        fontSize: 36,
        fontWeight: '600',
        marginTop: 27,
        color: colors.primary
    },
})
