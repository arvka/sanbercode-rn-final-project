import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { ContentHeader, ExternalIcon } from '../component'
import colors from '../utils/colors'


const Detail = ({route, navigation}) => {
    const {icon, name, desc} = route.params
    return (
        <View style={styles.container}>
            <ContentHeader />
            <View style={styles.cardWrapper}>
                <View style={styles.headerWrapper}>
                    <Text style={styles.headerCardText}>{name}</Text>
                </View>
                <ExternalIcon name={icon} size={150} color={colors.primary} />
                <View style={{flex: 1, padding: 15}}>
                <ScrollView style={{flex: 1}}>
                    <Text style={{color: colors.primary, fontSize: 14, fontWeight: '400', textAlign: 'justify'}}>{desc}</Text>
                </ScrollView>
                </View>
            </View>
        </View>
    )
}

export default Detail

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.bgGrey,
        flex: 1
    },
    cardWrapper: {
        backgroundColor: colors.bgWhite,
        marginHorizontal: 15,
        width: 'auto',
        flex: 1,
        borderRadius: 10,
        marginBottom: 30,
        marginTop: -60,
        alignItems: 'center',
        paddingBottom: 22,
        flexDirection: 'column'
    },
    headerCardText: {
        textAlign: 'center',
        fontSize: 18,
        fontWeight: '600'
    },
    headerWrapper: {
        width: '100%',
        paddingTop: 15,
        paddingBottom: 11,
        paddingHorizontal: 15,
        borderBottomColor: colors.borderGrey,
        borderBottomWidth: 2,
    }
})
