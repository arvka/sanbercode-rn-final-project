import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler'
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import { ContentHeader, SkillList } from '../component'
import colors from '../utils/colors'
import { showError } from '../utils/showMessage'
import database from '@react-native-firebase/database'

const Skills = ({navigation}) => {
    const [programming, setProgramming] = useState([])
    const [library, setLibrary] = useState([])
    const [tools, setTools] = useState([])
    const dispatch = useDispatch()
    useEffect(async () => {
        dispatch({type: 'SET_LOADING', value: true})
        try{
            await getSkills()
            dispatch({type: 'SET_LOADING', value: false})
        }catch(error){
            dispatch({type: 'SET_LOADING', value: false})
            showError(error.message)
        }
    }, [])

    const getSkills = () => (
        new Promise ((resolve, reject) => {
          database()
            .ref('skills/')
            .once('value')
            .then(res => {
              if(res.val()){
                const resdata = res.val()
                const filterProgramming = resdata.programming.filter(el => el != null)
                const filterLibrary = resdata.library.filter(el => el != null)
                const filterTools = resdata.tools.filter(el => el != null)
                // const filterData = resdata.filter(el => el != null)
                setProgramming(filterProgramming);
                setLibrary(filterLibrary);
                setTools(filterTools);
              }
              resolve()
            })
            .catch(error => {
              reject(error)
            });
        })
    )

    return (
        <View style={{flex: 1}}>
            <ScrollView showsHorizontalScrollIndicator style={styles.container} >
                <ContentHeader />
                <View style={{marginTop: -60}}>
                    <View style={styles.cardWrapper}>
                        <View style={styles.headerWrapper}>
                            <Text style={styles.headerCardText}>Programming Languages</Text>
                        </View>
                        <View style={{flexDirection: 'column'}}>
                            {programming.map(item => {
                                return (<SkillList key={item.id} iconName={item.icon} skillName={item.name} skillLevel={item.level} percent={item.percent} onPress={() => {navigation.navigate('Detail', item)}} />)
                            })}
                        </View>
                    </View>
                    <View style={styles.cardWrapper}>
                        <View style={styles.headerWrapper}>
                            <Text style={styles.headerCardText}>Framework / Library</Text>
                        </View>
                        <View style={{flexDirection: 'column'}}>
                            {library.map(item => {
                                return (<SkillList key={item.id} iconName={item.icon} skillName={item.name} skillLevel={item.level} percent={item.percent} onPress={() => {navigation.navigate('Detail', item)}} />)
                            })}
                        </View>
                    </View>
                    <View style={styles.cardWrapper}>
                        <View style={styles.headerWrapper}>
                            <Text style={styles.headerCardText}>Tools</Text>
                        </View>
                        <View style={{flexDirection: 'column'}}>
                            {tools.map(item => {
                                return (<SkillList key={item.id} iconName={item.icon} skillName={item.name} skillLevel={item.level} percent={item.percent} onPress={() => {navigation.navigate('Detail', item)}} />)
                            })}
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default Skills

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.bgGrey,
        flex: 1
    },
    cardWrapper: {
        backgroundColor: colors.bgWhite,
        marginHorizontal: 15,
        width: 'auto',
        minHeight: 120,
        borderRadius: 10,
        marginBottom: 20,
        alignItems: 'center',
        paddingBottom: 22
    },
    headerCardText: {
        textAlign: 'center',
        fontSize: 18,
        fontWeight: '600'
    },
    headerWrapper: {
        width: '100%',
        paddingTop: 15,
        paddingBottom: 11,
        paddingHorizontal: 15,
        borderBottomColor: colors.borderGrey,
        borderBottomWidth: 2,
    }
})
