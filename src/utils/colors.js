const colorPalette ={
    darkBlue: '#003366',
    lightBlue: '#19BAFF',
    white: '#FFFFFF',
    grey: '#C4C4C4',
    grey2: '#E5E5E5',
    black: '#000000',
    black2: 'rgba(0, 0, 0, 0.5)',
    red: '#ff5714',
    green: '#4cd477'
}

const colors = {
    primary : colorPalette.darkBlue,
    secondary: colorPalette.lightBlue,
    bgWhite: colorPalette.white,
    borderGrey: colorPalette.grey,
    bgGrey: colorPalette.grey2,
    loadingBackground: colorPalette.black2,
    error: colorPalette.red,
    success: colorPalette.green,
    black: colorPalette.black
}

export default colors;