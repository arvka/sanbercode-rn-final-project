import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Router from './router'
import { NavigationContainer } from '@react-navigation/native'
import { Provider, useSelector } from 'react-redux'
import FlashMessage from 'react-native-flash-message'
import { Loading } from './component'
import store from './redux/store'
import firebase from '@react-native-firebase/app'

const firebaseConfig = {
    apiKey: "AIzaSyDbmqFWAyZXYkQ9cN0fnQXeuvnMih0PM0A",
    authDomain: "portofolio-arka-1.firebaseapp.com",
    databaseURL: "https://portofolio-arka-1-default-rtdb.firebaseio.com",
    projectId: "portofolio-arka-1",
    storageBucket: "portofolio-arka-1.appspot.com",
    messagingSenderId: "1091661852283",
    appId: "1:1091661852283:web:888f0308644cd5a091bcde"
};

if(!firebase.apps.length){
    firebase.initializeApp(firebaseConfig)
}

const MainApp = () => {
    const stateLoading = useSelector(state => state)
    return (
        <>
            <NavigationContainer>
                <Router />
            </NavigationContainer>
            <FlashMessage position="top" />
            {/* <Loading /> */}
            {stateLoading.loading && <Loading />}
        </>
    )
}

const App = () => {
    return (
        <Provider store={store}>
            <MainApp />
        </Provider>
    )
}

export default App

const styles = StyleSheet.create({})
